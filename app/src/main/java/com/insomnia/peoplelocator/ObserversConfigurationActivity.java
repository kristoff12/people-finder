package com.insomnia.peoplelocator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.insomnia.peoplelocator.circles.CircleListAdapter;
import com.insomnia.peoplelocator.circles.EditObserverActivity;
import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.config.IntentMessages;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ObserversConfigurationActivity extends Activity implements ActionMode.Callback{
	
	private ArrayList<CircleItem> circleList = new ArrayList<CircleItem>();
	
	private Button cancelNewCircleButton;
	private Button saveNewCircleButtion;
	private LinearLayout newCircleButtonsLayout;
	private EditText newCircleEditText;
	
	// circle list adapter
	private CircleListAdapter circleListAdapter;
	
	private ListView circleListView;
	private LinearLayout viewLayout;
	
	private TextView hintTextView;
	
	private int selectedPosition;
	private ActionMode cabMode;
	
	private int selectedObservingCirclePosition = 0;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.observer_configuration_layout);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		// load the circle data from the preferences
		circleList = loadCircleList();
		
		// load the selected observing circle position
		SharedPreferences prefs = getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE);
		
		selectedObservingCirclePosition = prefs.getInt(ConfigurationTopics.SELECTED_OBSERVING_CIRLCE, 0);
		
		circleListView = (ListView)findViewById(R.id.circlesListView);
		circleListAdapter = new CircleListAdapter(this, circleList, selectedObservingCirclePosition);
		circleListView.setAdapter(circleListAdapter);
		
		viewLayout = (LinearLayout) findViewById(R.id.circles_layout);
		
		// hide the list view
		if (circleList.size() == 0){
			showHintTextView();
		}
		
		circleListView.setOnItemClickListener(new OnCirlceListItemClickListener(this));
		circleListView.setOnItemLongClickListener(new CircleItemChoiceListener(this));
		circleListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		
		// save the circle list via GSON
		Gson gson = new Gson();
		String circleListGson = gson.toJson(circleList);
		
		Editor editor = getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE).edit();
		
		editor.putString(ConfigurationTopics.CIRCLE_LIST_PREFERENCE_KEY, circleListGson);
		
		// save the selected observer circle
		editor.putInt(ConfigurationTopics.SELECTED_OBSERVING_CIRLCE, this.selectedObservingCirclePosition);
		editor.commit();
	}

	private ArrayList<CircleItem> loadCircleList() {
		
		Gson gson = new Gson();
		String circleListGson = getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE).getString(ConfigurationTopics.CIRCLE_LIST_PREFERENCE_KEY, "");
		
		ArrayList<CircleItem> deserializedCircleList = new ArrayList<CircleItem>();
		
		if (!circleListGson.equals("")){
			Type listOfTestObject = new TypeToken<ArrayList<CircleItem>>(){}.getType();
			deserializedCircleList = gson.fromJson(circleListGson, listOfTestObject);
		}
		
		return deserializedCircleList;
	}

	private void showHintTextView() {
		
		// hide the list view
		circleListView.setVisibility(View.INVISIBLE);
		
		hintTextView = new TextView(this);
		hintTextView.setText(R.string.addCircleHintText);
		hintTextView.setLayoutParams(new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT));
		hintTextView.setGravity(Gravity.CENTER);
        hintTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
		viewLayout.addView(hintTextView);
		
	}

	public void addContactButtonClicked(View view){
		
		newCircleEditText = new EditText(this);
		
		// add exit text view for new circle below circles text
		newCircleEditText.requestFocus();
		viewLayout.addView(newCircleEditText, 1);
		
		// show the visual keyboard
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(INPUT_METHOD_SERVICE);
		inputManager.showSoftInput(newCircleEditText, InputMethodManager.SHOW_IMPLICIT);
		
		// show two button below the edit text (cancel/ save)
		newCircleButtonsLayout = new LinearLayout(this);		
		newCircleButtonsLayout.setOrientation(LinearLayout.HORIZONTAL);
		
		LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, 
				LinearLayout.LayoutParams.WRAP_CONTENT);
		p.weight = 0.5f;

		cancelNewCircleButton = new Button(this);
		cancelNewCircleButton.setText(R.string.cancel_button_text);
		cancelNewCircleButton.setLayoutParams(p);
		
		saveNewCircleButtion = new Button(this);
		saveNewCircleButtion.setText(R.string.save_button_text);
		saveNewCircleButtion.setLayoutParams(p);
		
		newCircleButtonsLayout.addView(cancelNewCircleButton);
		newCircleButtonsLayout.addView(saveNewCircleButtion);
		viewLayout.addView(newCircleButtonsLayout,2);
		
		NewCircleOnClickListener onClickListener = new NewCircleOnClickListener();
		
		cancelNewCircleButton.setOnClickListener(onClickListener);
		saveNewCircleButtion.setOnClickListener(onClickListener);
		
	}
	
	private void handleUserSavedNewCircle(){
		String newCircleName = newCircleEditText.getText().toString();
		removeAddNewCircleInterface();
		
		if (circleList.size() == 0){
			// hide the hint text view and show the list view
			hintTextView.setVisibility(View.INVISIBLE);
			circleListView.setVisibility(View.VISIBLE);
		}
		
		addNewCircleToList( newCircleName);
		circleListAdapter.notifyDataSetChanged();
	}
	
	private void addNewCircleToList(String pNewCircleName) {
		
		CircleItem circleItem = new CircleItem(pNewCircleName);
		circleList.add(circleItem);	
	}

	private void handleUserCancelledNewCircleCreation(){
				
		removeAddNewCircleInterface();
	}
	
	private void removeAddNewCircleInterface() {
		
		LinearLayout viewLayout = (LinearLayout) findViewById(R.id.circles_layout);
		
		// remove the cancel/ save buttons
		viewLayout.removeView(newCircleButtonsLayout);
		newCircleButtonsLayout = null;
		
		// hide the soft keyboard
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(newCircleEditText.getWindowToken(), 
				0);
		
		// remove the edit text view
		viewLayout.removeView(newCircleEditText);
		newCircleEditText = null;
	}
	
	public void deleteSelectedItems(){
		
		// check if all circles were deleted
		if (circleList.size() == 1){
			// show the "add circles info"
			showHintTextView();
		}
		
		circleList.remove(selectedPosition);
		
		circleListAdapter.notifyDataSetChanged();
	}
	
	public void itemChecked( View view, int position, boolean state) {
		
		String observationMessage = getString(R.string.selected_observing_circle_text);
		selectedObservingCirclePosition = position;
		
		// show information about the observer circle to the user
		Toast.makeText(this, observationMessage + " " + circleList.get(position).mCircleName, Toast.LENGTH_SHORT).show();
	}

	/**
	 * Inner class for handling the new circle 
	 * @author Krzysiek
	 *
	 */
	class NewCircleOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			if (v == cancelNewCircleButton){
				Log.d(getClass().toString(), "Cancel button clicked");
				handleUserCancelledNewCircleCreation();
			} else if (v == saveNewCircleButtion){
				Log.d(getClass().toString(), "Save new button clicked");
				handleUserSavedNewCircle();
			}
			
		}
		
	}
	
	class OnCirlceListItemClickListener implements OnItemClickListener {

		Context context;
		
		public OnCirlceListItemClickListener(Context pContext) {
			context = pContext;
		}
		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			
			CircleItem circleItem = (CircleItem)parent.getItemAtPosition(position);
			
			Log.d(getClass().toString(), "You selected circle: " + circleItem.getCircleName());
			Intent intent = new Intent(context, EditObserverActivity.class);
			intent.putExtra(IntentMessages.CircleItem, circleItem);
			startActivity(intent);
			
		}
		
	}
	
	public class CircleItemChoiceListener implements OnItemLongClickListener{

		private Activity context;
		
		public CircleItemChoiceListener(Activity pActivity){
			context = pActivity;
		}

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
			
			   if(cabMode != null)
			        return false;
			    selectedPosition = position;
			    circleListView.setItemChecked(position, true);

			    circleListView.setOnItemClickListener(null);
			    cabMode = context.startActionMode((Callback) context);
			    return true;
		}
		
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Inflate the menu for the CAB
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.circle_config_menu, menu);
        return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        // Respond to clicks on the actions in the CAB
        switch (item.getItemId()) {
            case R.id.context_menu_circle_remove:
                deleteSelectedItems();
                mode.finish(); // Action picked, so close the CAB
                return true;
            default:
                return false;
        }
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		
	    cabMode = null;
	    circleListView.setItemChecked(this.selectedPosition, false);
	    selectedPosition = -1;
		circleListView.setOnItemClickListener(new OnCirlceListItemClickListener(this));
	}
}
