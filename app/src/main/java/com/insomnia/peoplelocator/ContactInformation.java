package com.insomnia.peoplelocator;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class represents a contact
 * @author Krzysiek
 *
 */
public class ContactInformation implements Parcelable{
	private String mContactName;
	private String mPhoneNumber;
	private long mContactID;
	
	public ContactInformation( String pContactName){
		this.mContactName = pContactName;
	}
	
	public ContactInformation(Parcel source) {
		mContactName = source.readString();
		mPhoneNumber = source.readString();
		mContactID = source.readLong();
	}

	public String getContactName(){
		return mContactName;
	}

	public void setPhoneNumber(String pPhoneNumber) {
		this.mPhoneNumber = pPhoneNumber;
	}
	
	public String getPhoneNumber(){
		return this.mPhoneNumber;
	}
	
	public void setContactID (long pContactID){
		mContactID = pContactID;
	}
	
	public long getContactID (){
		return mContactID;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mContactName);
		dest.writeString(mPhoneNumber);
		dest.writeLong(mContactID);
	}
	
	public static final Parcelable.Creator<ContactInformation> CREATOR = new Parcelable.Creator<ContactInformation>() {

		@Override
		public ContactInformation createFromParcel(Parcel source) {
			return new ContactInformation(source);
		}

		@Override
		public ContactInformation[] newArray(int size) {
			return new ContactInformation[size];
		}
	};

}
