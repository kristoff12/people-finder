package com.insomnia.peoplelocator;

import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.PhoneLookup;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.insomnia.peoplelocator.common.PersonLocation;
import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.config.IntentMessages;
import com.insomnia.peoplelocator.config.WebAPIConstants;
import com.insomnia.peoplelocator.map.MapBroadcastReceiver;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class LocationMapFragment extends Fragment implements OnMapReadyCallback{

	private SupportMapFragment fragment;
	private GoogleMap map;
	private Marker hostPositionMarker;
	private boolean isMapReady = false;
	private HashMap<Long, Marker> contactMarkers;
	
	// service broadcast receiver
	MapBroadcastReceiver mapBroadcastReceiver;
	
	UpdateObserverHandler observerUpdateHandler;
	
    @Override
    public void onMapReady(GoogleMap pMap) {
    	
    	isMapReady = true;
    	map = pMap;
    	
    	ScheduledExecutorService scheduler =
    		    Executors.newSingleThreadScheduledExecutor();
    	
    	// get the position of people that would like me to watch them
    	scheduler.scheduleAtFixedRate(new UpdateObserverPeopleLocationTask(), 0, 5, TimeUnit.MINUTES);    	
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		mapBroadcastReceiver = new MapBroadcastReceiver(this);
		observerUpdateHandler = new UpdateObserverHandler();
		
		setupParse();
		
		contactMarkers = new HashMap<Long, Marker>();
		
		return inflater.inflate(R.layout.mapfragment, container, false);
	}
	
	private void setupParse() {
		// setup Parse
		Parse.initialize(getActivity(), "sOsBqqe5p4RUivGYhFH5x6WOtFCHawnYHhbNdtZa", 
				"KCsoM8ABjcUi8SbJTEa03vURaSjIqEJe0jJN4T8R");
		
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		FragmentManager fm = getChildFragmentManager();
		fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
		if (fragment == null) {
			fragment = SupportMapFragment.newInstance();
			fm.beginTransaction().replace(R.id.map, fragment).commit();
		}
		
		fragment.getMapAsync(this);
	}

	public void updateHostLocation(
			Location pLocation) {
		
		if(isMapReady){
			setHostPositionMarker(
					pLocation.getLongitude(),
					pLocation.getLatitude());
		}
		
	}

	private void setHostPositionMarker(
		double pLongitude,
		double pLatitude) {
		
		if (hostPositionMarker != null){
			
			hostPositionMarker.setPosition(new LatLng(
					pLatitude, 
					pLongitude));
			
		} else {
		
			hostPositionMarker = map.addMarker(
				new MarkerOptions().position(
						new LatLng(
								pLatitude, 
								pLongitude)).title("My Position").icon(
										BitmapDescriptorFactory.fromResource(
												R.drawable.user_person_icon )));
		}
		
		LinkedList<Marker> markers = new LinkedList<Marker>(contactMarkers.values());
		markers.add(hostPositionMarker);
		zoomMapToSeeAllMarkers(markers);
	}

	public void handleLocationUpdate(List<PersonLocation> personLocationList) {
		for(PersonLocation personLocation : personLocationList){
			if (contactMarkers.containsKey(personLocation.getContactInformation().getContactID())){
				
				// update marker
				Marker marker = contactMarkers.get(
						personLocation.getContactInformation().getContactID());
				marker.setPosition(
						new LatLng(
								personLocation.getLatitude(),
								personLocation.getLongitude()));
			} else {
				// create marker
				Marker marker = map.addMarker(
						new MarkerOptions().position(
								new LatLng(
										personLocation.getLatitude(), 
										personLocation.getLongitude())).title(
												personLocation.getContactInformation().getContactName()).icon(
														BitmapDescriptorFactory.fromResource(
																R.drawable.person_icon)));
				
				// add to list
				contactMarkers.put(
						personLocation.getContactInformation().getContactID(), 
						marker);
			}
		}
		
		// update map to see all markers
		zoomMapToSeeAllMarkers(contactMarkers.values());
		
	}
	
	private void zoomMapToSeeAllMarkers(
			Collection<Marker> pMarkerList) {
		
		if (pMarkerList.size() > 0){
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			for (Marker marker : pMarkerList) {
			    builder.include(marker.getPosition());
			}
			
			if (hostPositionMarker != null){
				// add host marker
				builder.include(hostPositionMarker.getPosition());
			} else if (pMarkerList.size() == 1){
				
				// host marker not yet available and there is only one marker
				// with a tracked person
				map.animateCamera(
						CameraUpdateFactory.newLatLngZoom(
								pMarkerList.iterator().next().getPosition(),
								12.0f));
			}
			
			LatLngBounds bounds = builder.build();
			
			int padding = 50; // offset from edges of the map in pixels
			CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
			map.animateCamera(cu);	
		}
	}

	@Override
	public void onResume() {
		
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mapBroadcastReceiver,
			      new IntentFilter(IntentMessages.HostLocation));
		
		super.onResume();
	}
	
	@Override
	public void onPause() {
		
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mapBroadcastReceiver);
		
		super.onPause();
	}
	
	List<PersonLocation> parseParseObjectListToPersonLocationList (
			List<ParseObject> parseObjectList){
		
		List<PersonLocation> output = new ArrayList<PersonLocation>();
		
		for(ParseObject parseObject : parseObjectList){
			double latitude = (Double)parseObject.get(WebAPIConstants.LATITUDE_KEY);
			double longitude = (Double)parseObject.get(WebAPIConstants.LONGITUDE_KEY);
			String senderNumber = (String)parseObject.get(WebAPIConstants.SENDING_PHONE_NUMBER);
			
			String senderName = getContactName(getActivity(), senderNumber);
			
			if (senderName != null){
				PersonLocation personLocation = new PersonLocation(senderName);
				personLocation.setLatitude(latitude);
				personLocation.setLongitude(longitude);
				personLocation.getContactInformation().setContactID(
						Long.parseLong(
								parseObject.getObjectId(), 
								36));
			
				output.add(personLocation);
			}
		}
		
		return output;	
	}

	public static String getContactName(Context context, String phoneNumber) {
	    ContentResolver cr = context.getContentResolver();
	    Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
	    Cursor cursor = cr.query(uri, new String[]{PhoneLookup.DISPLAY_NAME}, null, null, null);
	    if (cursor == null) {
	        return null;
	    }
	    String contactName = null;
	    if(cursor.moveToFirst()) {
	        contactName = cursor.getString(cursor.getColumnIndex(PhoneLookup.DISPLAY_NAME));
	    }

	    if(cursor != null && !cursor.isClosed()) {
	        cursor.close();
	    }

	    return contactName;
	}

	class UpdateObserverPeopleLocationTask implements Runnable {
		@Override
		public void run(){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("LocationObject");
			query.whereEqualTo(
					WebAPIConstants.RECEIVING_PHONE_NUMBER_ARRAY, 
					getMyPhoneNumber());
			query.findInBackground(new FindCallback<ParseObject>() {
			    public void done(List<ParseObject> scoreList, ParseException e) {
			        if (e == null) {
			            Log.d("observer_info", "Retrieved " + scoreList.size() + " entries");
			            List<PersonLocation> personLocationList = 
			            		parseParseObjectListToPersonLocationList(scoreList);
			            
			            handleLocationUpdate(personLocationList);
			        } else {
			            Log.d("observer_info", "Error: " + e.getMessage());
			        }
			    }
			});
		}

		private String getMyPhoneNumber() {
			
			SharedPreferences prefs = getActivity().getSharedPreferences(
					ConfigurationTopics.USER_PHONE_CONFIG, 0);
			
			String number = prefs.getString(ConfigurationTopics.USER_PHONE_NUMBER, "");
			return number;
		}
	}
	
	class UpdateObserverHandler extends Handler{
		
        @Override
        public void handleMessage(Message msg)
        {
            msg.getData();
            
        }
	}
	
}
