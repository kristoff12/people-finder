package com.insomnia.peoplelocator.common;

public class PhoneNumberConverter {
	
	/**
	 * Method converts a phone number to a number
	 * without +xx and spaces - only last 9 digits preserved
	 * @param pNumber
	 * @return
	 * @throws Exception when erorr detected like alphabetic chars
	 */
	public static String convertPhoneNumber(
			String pDirection,
			String pNumber) throws Exception{
		
		String tempDirection = pDirection.replace(" ","");
		String tempNumber = pNumber.replace(" ","");
		
		
		// direction should contain only digits
		if (!tempDirection.matches("[0-9]+")){
			throw new Exception();
		}
		
		// number should contain only digits
		if (!tempNumber.matches("[0-9]+")){
			throw new Exception();
		}
		
		if (tempNumber.length() != 9){
			throw new Exception();
		}
		
		// format the final form
		// +XX XXX XXX XXX
		StringBuilder builder = new StringBuilder();
		builder.append("+");
		builder.append(tempDirection);
		builder.append(" ");
		builder.append(pNumber.substring(0, 3));
		builder.append(" ");
		builder.append(pNumber.substring(3, 6));
		builder.append(" ");
		builder.append(pNumber.substring(6, 9));
		
		return builder.toString();	
	}

}
