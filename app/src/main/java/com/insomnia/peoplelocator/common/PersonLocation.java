package com.insomnia.peoplelocator.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.insomnia.peoplelocator.ContactInformation;

public class PersonLocation implements Parcelable{
	
	double mLongitude;
	double mLatitude;
	ContactInformation mContactInformation;
	
	public PersonLocation (String pName){
		mContactInformation = new ContactInformation(pName);
		mLatitude =0;
		mLongitude =0;
	}
	
	public PersonLocation(Parcel source) {
		
		mContactInformation = source.readParcelable(null);
		mLatitude = source.readDouble();
		mLongitude = source.readDouble();
	}

	public ContactInformation getContactInformation(){
		return mContactInformation;
	}
	
	public void setContact(ContactInformation pContact){
		mContactInformation = pContact;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(mContactInformation, 0);
		dest.writeDouble(mLatitude);
		dest.writeDouble(mLongitude);
	}
	
	public static final Parcelable.Creator<PersonLocation> CREATOR = new Parcelable.Creator<PersonLocation>() {

		@Override
		public PersonLocation createFromParcel(Parcel source) {
			return new PersonLocation(source);
		}

		@Override
		public PersonLocation[] newArray(int size) {
			return new PersonLocation[size];
		}
	};

	public double getLongitude(){
		return mLongitude;
	}
	
	public double getLatitude(){
		return mLatitude;
	}
	
	public void setLongitude( double pValue){
		mLongitude = pValue;
	}
	
	public void setLatitude( double pValue){
		mLatitude = pValue;
	}

}
