package com.insomnia.peoplelocator;

import android.app.Fragment;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.insomnia.peoplelocator.config.ConfigurationTopics;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class ObserversFragment extends Fragment implements OnClickListener{

	private ImageSwitcher imageSwitcher;
	private ArrayList<Bitmap> contactImages = new ArrayList<Bitmap>();
	
	private ArrayList<CircleItem> mCircles;
	private int mWatchingCirclePosition;
	
	private Timer imageChangeTimer;
	
	TextView observingCircleNameTextView;
    TextView observersLabelTextView;
	
	public ObserversFragment() {
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.observer_fragment, container, false);
        rootView.setOnClickListener(this);
        
        observingCircleNameTextView = (TextView)rootView.findViewById(R.id.observingCircleNameLabel);
        observersLabelTextView = (TextView)rootView.findViewById(R.id.observersLabel);
        
        return rootView;
    }

	private ArrayList<Bitmap> loadObservingCircleImages() {
		
		if( mWatchingCirclePosition < mCircles.size()){
			CircleItem watchingCircle = mCircles.get(mWatchingCirclePosition);
			
			for(ContactInformation contactInfo : watchingCircle.mContactList){
				contactImages.add(loadContactImage(contactInfo.getContactID()));
			}
			
			if (contactImages.size() == 0){
				// no images for contacts in circle
				// add generic image
				contactImages.add(
						BitmapFactory.decodeResource(
								getResources(), 
								R.drawable.people_group));
			}
		} else {
            // no circle created - show hint for the user
            contactImages.add(
                    BitmapFactory.decodeResource(
                            getResources(),
                            R.drawable.add_user));
        }
		
		return contactImages;
	}

	@Override
	public void onClick(View v) {

		Log.d(this.getClass().toString(), "Observer fragment clicked");
		
		// launch the observer configuration activity
		Intent intent = new Intent(getActivity(),ObserversConfigurationActivity.class);
		startActivity(intent);
	}
	
	public class ImageChangeTimer extends TimerTask{

		int currentImage = 0;
		
		final Handler myHandler = new Handler() {
            @Override
            public void handleMessage(Message msg)
            {
                /*do all your ui action here to display the image ()*/
				imageSwitcher.setImageDrawable(
						new BitmapDrawable(
								getResources(),contactImages.get(currentImage)));
            }
        };
		
		@Override
		public void run() {
			if (contactImages.size() > 0){
				
				myHandler.sendEmptyMessage(0);
				currentImage++;
				currentImage = currentImage % contactImages.size();
			}
		}
		
	}
	
	private ArrayList<CircleItem> loadCircleList() {

		Gson gson = new Gson();
		String circleListGson = getActivity().getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE).getString(ConfigurationTopics.CIRCLE_LIST_PREFERENCE_KEY, "");
		
		ArrayList<CircleItem> deserializedCircleList = new ArrayList<CircleItem>();
		
		if (!circleListGson.equals("")){
			Type listOfTestObject = new TypeToken<ArrayList<CircleItem>>(){}.getType();
			deserializedCircleList = gson.fromJson(circleListGson, listOfTestObject);
		}
		
		return deserializedCircleList;
	}
	
	private int loadWatchingCirclePosition() {
		
		// load the selected observing circle position
		SharedPreferences prefs = getActivity().getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE);
		
		return prefs.getInt(ConfigurationTopics.SELECTED_OBSERVING_CIRLCE, 0);
	}
	
	 private Bitmap loadContactImage(long contactId) {
	     Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
	     Uri photoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.CONTENT_DIRECTORY);
	     Cursor cursor = getActivity().getContentResolver().query(photoUri,
	          new String[] {Contacts.Photo.PHOTO}, null, null, null);
	     if (cursor == null) {
	         return null;
	     }
	     try {
	         if (cursor.moveToFirst()) {
	             byte[] data = cursor.getBlob(0);
	             if (data != null) {
	                 return BitmapFactory.decodeByteArray(data, 0, data.length);
	             }
	         }
	     } finally {
	         cursor.close();
	     }
	     return null;
	 }
	 
	 @Override
	public void onPause() {
		
		// kill the timer
		imageChangeTimer.cancel();
		 
		super.onPause();
	}
	 
	 @Override
	public void onStop() {
		
		// clear the images
		contactImages.clear();
		 
		super.onStop();
	}
	 
	@Override
	public void onResume() {
		
		// load the circle information
		loadCircleInformation();
		
		// set the circle name
		if (mCircles.size() > 0){
			observingCircleNameTextView.setText(
					mCircles.get(mWatchingCirclePosition).getCircleName());
            observersLabelTextView.setText(R.string.selected_circle_label);
		} else {
            // show hint to the user
            observersLabelTextView.setText(R.string.create_circle_hint_text);
            observingCircleNameTextView.setText("");
        }
		
        imageSwitcher = (ImageSwitcher) getView().findViewById(R.id.imageSwitcher);
		imageSwitcher.removeAllViews();
        
        contactImages = loadObservingCircleImages();
        
        imageSwitcher.setFactory(new ViewFactory() {
            
            public View makeView() {
                
                    // Create a new ImageView set it's properties 
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView.setLayoutParams(new ImageSwitcher.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
                    return imageView;
            }
        });

        // Declare the animations and initialize them
        Animation in = AnimationUtils.loadAnimation(getActivity(),android.R.anim.fade_in);
        Animation out = AnimationUtils.loadAnimation(getActivity(),android.R.anim.fade_out);
        
        // set the animation type to imageSwitcher
        imageSwitcher.setInAnimation(in);
        imageSwitcher.setOutAnimation(out);
        
        imageChangeTimer = new Timer();
        imageChangeTimer.scheduleAtFixedRate(new ImageChangeTimer(), 0, 5000);
        
		super.onResume();
	}

	private void loadCircleInformation() {
		
		mCircles = loadCircleList();
		mWatchingCirclePosition = loadWatchingCirclePosition();
		
	}
}
