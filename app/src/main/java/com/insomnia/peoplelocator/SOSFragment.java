package com.insomnia.peoplelocator;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.tracking.PeopleFinderService;

import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class SOSFragment extends Fragment {

    private int mSOSCounterValue = 5;
    private TextView mSOSCounterTextView;

    private Timer sosCountdownTimer;
    private TextView mSOSInfoTextView;


    public SOSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.sos_fragment_layout, container, false);
    }

    @Override
    public void onResume() {

        mSOSCounterTextView = (TextView)(getView().findViewById(R.id.SOS_message_countdown_timer));
        mSOSInfoTextView = (TextView)(getView().findViewById(R.id.SOS_message_label));

        // start timer
        sosCountdownTimer = new Timer();
        sosCountdownTimer.scheduleAtFixedRate(new SOSCountDownTimerTick(), 0, 1000);

        super.onResume();
    }

    @Override
    public void onPause() {

        // kill the timer
        sosCountdownTimer.cancel();

        super.onPause();
    }

    public class SOSCountDownTimerTick extends TimerTask {

        final Handler myHandler = new Handler() {
            @Override
            public void handleMessage(Message msg)
            {
                if (msg.what == 0) {
                    --mSOSCounterValue;
                    mSOSCounterTextView.setText(Integer.toString(mSOSCounterValue));
                } else {
                    // time is up
                    mSOSCounterTextView.setText("");
                    mSOSInfoTextView.setText(R.string.sos_message_sent_text);

                }
            }
        };

        @Override
        public void run() {
            if (mSOSCounterValue > 0) {
                myHandler.sendEmptyMessage(0);
            }else {
                myHandler.sendEmptyMessage(1);

                // send SOS sending info to tracking service
                Intent serviceIntent = new Intent(getActivity(), PeopleFinderService.class);
                serviceIntent.putExtra(ConfigurationTopics.SEND_SOS_MESSAGE, true);
                getActivity().startService(serviceIntent);

                this.cancel();
            }
        }

    }
}
