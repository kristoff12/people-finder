package com.insomnia.peoplelocator;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.config.IntentMessages;
import com.insomnia.peoplelocator.tracking.AskForLocationServicesDialogFragment;
import com.insomnia.peoplelocator.tracking.FetchAddressIntentService;
import com.insomnia.peoplelocator.tracking.PeopleFinderService;
import com.insomnia.peoplelocator.tracking.PeopleFinderServiceConnection;
import com.insomnia.peoplelocator.tracking.ServiceBroadcastReceiver;

public class TrackingStatusFragment extends Fragment implements
		OnClickListener {

	private SharedPreferences sharedPreferences;
	private boolean trackingEnabled = false;
	private double currentLongitude = 0;
	private double currentLatitude = 0;

	// location tracking
	private TextView longitudeStatusText;
	private TextView latitudeStatusText;
    private TextView youLocationLabelTextView;

    private AddressResultReceiver mResultReceiver;
    private String addressInfo;
    private int reverseGeocodingResultCode;

	// service connection
	private PeopleFinderServiceConnection mConnection;
	
	// service broadcast receiver
	ServiceBroadcastReceiver serviceBroadcastReceiver;
	
	// image view
	ImageView satelliteImageView;

	public TrackingStatusFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.tracking_status_fragment,
				container, false);
		LinearLayout mainLayout = (LinearLayout) rootView
				.findViewById(R.id.trackingFragmentLayout);
		mainLayout.setOnClickListener(this);
		
		satelliteImageView = (ImageView) rootView.findViewById(R.id.satellite_image);

		sharedPreferences = getActivity().getPreferences(0);
		trackingEnabled = sharedPreferences.getBoolean(
				ConfigurationTopics.TRACKING_ENABLED, false);
		
		serviceBroadcastReceiver = new ServiceBroadcastReceiver(this);

        mResultReceiver = new AddressResultReceiver(new Handler());
		
		// create tracking service upon first run
		startTrackingService();
		
		return rootView;
	}

	private void startTrackingService() {

		if (trackingEnabled){
		
			// if tracking is enabled start the tracking service
			
			// first ckeck if service is already running
			if (isMyServiceRunning(PeopleFinderService.class)){
				killTrackingService();
			}
			
			// load service configuration
			int preferedLocationSendMethod = sharedPreferences.getInt(
					ConfigurationTopics.PREFERED_LOCATION_SEND_METHOD_TYPE, 0);
			
			Log.d(this.getClass().toString(), "Starting tracking service");
			Intent serviceIntent = new Intent(getActivity(),
					PeopleFinderService.class);
			
			serviceIntent.putExtra(
					ConfigurationTopics.PREFERED_LOCATION_SEND_METHOD_TYPE, preferedLocationSendMethod);
			
			serviceIntent.putExtra(
					ConfigurationTopics.PREFERED_LOCATION_SEND_METHOD_TYPE, preferedLocationSendMethod);
			
			getActivity().startService(serviceIntent);
		}

	}

	public void onResume() {
	
		// restore the state
		restoreUIState(getView());

		if (trackingEnabled) {

			connectToTrackingService();
		}
		
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(serviceBroadcastReceiver,
			      new IntentFilter(IntentMessages.GPS_ACTIVATION_REQUEST));
		
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(serviceBroadcastReceiver,
			      new IntentFilter(IntentMessages.LOCATION_UPDATE));
		
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(serviceBroadcastReceiver,
			      new IntentFilter(IntentMessages.USER_REFUSED_ACTIVATING_LOCATION_SERVICES));
		
		super.onResume();
	}

	/**
	 * Method connects to the tracking service. First we check if the location services are on
	 */
	private void connectToTrackingService() {
		
		Log.d(this.getClass().toString(), "binding to tracking service");
		
		// create service connection object
		mConnection = new PeopleFinderServiceConnection();
		
		// bind to tracking service
		Intent serviceIntent = new Intent(getActivity(),
				PeopleFinderService.class);
		getActivity().bindService(serviceIntent, mConnection,
				Context.BIND_AUTO_CREATE);
		
	}

	// method prompts the user to activate the GPS and sends him to the settings
	// window
	public void promptUserToActivateGPS() {

		Log.d(this.getClass().toString(),
				"Location services not enabled asking user for action...");
		AskForLocationServicesDialogFragment fragment = new AskForLocationServicesDialogFragment();
		fragment.show(getActivity().getFragmentManager(),
				"LocationServicesQuestion");
	}

	// method restores the UI data from before closing the application
	private void restoreUIState(View pView) {

		if (null == sharedPreferences){
			sharedPreferences = getActivity().getPreferences(0);
		}

		currentLongitude = Double.longBitsToDouble(sharedPreferences.getLong(
				ConfigurationTopics.STORED_LONGITUDE, 0L));

		currentLatitude = Double.longBitsToDouble(sharedPreferences.getLong(
				ConfigurationTopics.STORED_LATITUDE, 0L));

		longitudeStatusText = (TextView) pView
				.findViewById(R.id.wysokosc_label);
		latitudeStatusText = (TextView) pView
				.findViewById(R.id.szerokosc_label);

        youLocationLabelTextView = (TextView) pView
                .findViewById(R.id.youLocationLabel);

		updateTrackingStatusView(pView);
	}

	private void updateTrackingStatusView(View pView) {

		TextView trackingStatusText = (TextView) pView
				.findViewById(R.id.tracking_status_text);

		if (trackingEnabled) {
			trackingStatusText.setText(R.string.tracking_status_active);
			
			// set the icon
			satelliteImageView.setImageResource(R.drawable.satellite_active);
		} else {
			trackingStatusText.setText(R.string.tracking_status_inactive);
			
			// set the icon
			satelliteImageView.setImageResource(R.drawable.satellite_inactive);
		}

		updateLocationGUIText();
	}

	private void updateLocationGUIText() {

        if (currentLongitude == 0.0 && currentLatitude == 0.0){
            // initial text
            youLocationLabelTextView.setText(R.string.tracking_not_launched_label1);
            latitudeStatusText.setText("");
            longitudeStatusText.setText("");

        } else if (addressInfo != null &&
                reverseGeocodingResultCode != IntentMessages.FAILURE_RESULT){

            youLocationLabelTextView.setText(R.string.current_location_fragment_label);
            latitudeStatusText.setText(addressInfo);
            longitudeStatusText.setText("");
        } else {

            // error occurred show the coordinates
            String longitudeString = Double.toString(currentLongitude);
            String latitudeString = Double.toString(currentLatitude);

            if (longitudeString.length() > 8) {
                longitudeStatusText.setText(longitudeString.substring(0, 8));
            } else {
                longitudeStatusText.setText(longitudeString);
            }

            if (latitudeString.length() > 8) {
                latitudeStatusText.setText(latitudeString.substring(0, 8));
            } else {
                latitudeStatusText.setText(latitudeString);
            }

            youLocationLabelTextView.setText(R.string.current_location_fragment_label);
        }
    }

	@Override
	public void onClick(View v) {
		Log.d(this.getClass().toString(), "kliknieto sledzenie");

		trackingEnabled = !trackingEnabled;
		updateTrackingStatusView(v);

		if (trackingEnabled) {
			// launch tracking service
			Log.d(this.getClass().toString(), "enabling tracking");
			startTrackingService();
			
			connectToTrackingService();
		} else {
			// terminate tracking
			Log.d(this.getClass().toString(), "disabling tracking");
			
			// disconnect from service
			disconnectFromService();
					
			// kill tracking service
			killTrackingService();
		}
	}

	private void disconnectFromService() {
		if (null != mConnection){
			getActivity().unbindService(mConnection);
			mConnection = null;	
		}
	}

	private void killTrackingService() {
		Intent serviceIntent = new Intent(getActivity(),
				PeopleFinderService.class);
		getActivity().stopService(serviceIntent);	
	}

	public void onPause() {
		
		// disconnect from service
		disconnectFromService();
		
		// unregister the broadcast receiver
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(serviceBroadcastReceiver);

		// save the status of the UI
		if (null != sharedPreferences) {
			Editor editor = sharedPreferences.edit();
			editor.putBoolean(ConfigurationTopics.TRACKING_ENABLED,
					trackingEnabled);
			editor.putLong(ConfigurationTopics.STORED_LONGITUDE,
					Double.doubleToRawLongBits(currentLongitude));
			editor.putLong(ConfigurationTopics.STORED_LATITUDE,
					Double.doubleToRawLongBits(currentLatitude));
			editor.commit();
		}
		
		super.onPause();
	}


	/**
	 * Metoda przetwarza otrzymaną zaktualizowaną pozycję urządzenia
	 * @param pLocation zaktualizowana pozycja
	 */
	public void handleLocationUpdate(Location pLocation) {

		if (null != pLocation) {
            currentLongitude = pLocation.getLongitude();
            currentLatitude = pLocation.getLatitude();

            // perform reverse geocoding
            Intent intent = new Intent(getActivity(), FetchAddressIntentService.class);
            intent.putExtra(IntentMessages.RECEIVER, mResultReceiver);

            double[] coords = {currentLatitude, currentLongitude};

            intent.putExtra(IntentMessages.LOCATION_DATA_EXTRA, coords);
            getActivity().startService(intent);
		}
	}
	
	public void handleUserRefusedToActivateGPS(){
		
		// tracking was not activated
		trackingEnabled = false;
		
		// terminate tracking
		Log.d(this.getClass().toString(), "user did not activate location services disabling tracking");
		
		// disconnect from service
		disconnectFromService();
				
		// kill tracking service
		killTrackingService();
		
		updateTrackingStatusView(getView());
	}
	
	/**
	 * Method checks if a service is running or not
	 * @param serviceClass the class of the service
	 * @return if the service is running
	 */
	private boolean isMyServiceRunning(Class<?> serviceClass) {
	    ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}

	public double[] getCurrentLocation() {
		double[] output = {currentLatitude, currentLongitude};
		return output;
	}

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string
            // or an error message sent from the intent service.
            addressInfo = resultData.getString(IntentMessages.RESULT_DATA_KEY);
            reverseGeocodingResultCode = resultCode;
            updateLocationGUIText();
        }
    }
}
