package com.insomnia.peoplelocator.tracking;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.insomnia.peoplelocator.CircleItem;
import com.insomnia.peoplelocator.ContactInformation;
import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.config.IntentMessages;
import com.insomnia.peoplelocator.config.WebAPIConstants;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PeopleFinderService extends Service implements LocationListener, OnSharedPreferenceChangeListener{

	private final IBinder mBinder = new PeopleFinderServiceBinder();
	private float currentBestAccuracy;
	private LocationManager locationManager;
	private String locationProvider;
	private int numberInternetMessagesPerHour;
	private int numberSMSMessagesPerHour;
	
	// current location
	private double currentLongitude = 0;
	private double currentLatitude = 0;
	
	// sms sending
	private Timer smsTimer;
	
	// internet sending
	private Timer internetMessageTimer;
	
	// flag determining if someone is bound to the service
	private boolean serviceBound = false; 
	

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		if (intent != null){
			if (intent.getExtras().containsKey(
					ConfigurationTopics.SEND_LOCATION_TO_CIRCLE_AFTER_CALL) &&
					intent.getExtras().getBoolean(
							ConfigurationTopics.SEND_LOCATION_TO_CIRCLE_AFTER_CALL) == true){
				
				String callerNumber = intent.getExtras().getString(ConfigurationTopics.CALLER_NUMBER);
				boolean sendSMSOnNoAnswer = getBooleanFromPreferences(ConfigurationTopics.SEND_SMS_ON_CALL_NO_ANSWER);
				sendLocationToCallingNumber(sendSMSOnNoAnswer, true, callerNumber);
				
			}else if (intent.getExtras().containsKey(
                    ConfigurationTopics.SEND_SOS_MESSAGE)){

                // send SOS message to watching circles
                sendSOSMessageToWatchingCircles();

                // save that tracking was enabled
                saveBooleanToPreferences(ConfigurationTopics.TRACKING_ENABLED, true);
			}
		}
		
		return Service.START_STICKY;
	}

    private void sendSOSMessageToWatchingCircles(){

        Timer sosMessageTimer = new Timer();

        sosMessageTimer.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                if (currentLatitude != 0 && currentLongitude != 0){
                    Log.d(this.getClass().toString(),
                            "SOS message triggered with location " + currentLongitude +
                                    "; " + currentLatitude);

                    sendLocationToWatchingCircles(true,true,true);

                    this.cancel();
                }
            }
        }, 0, 500);
    }

    private boolean getBooleanFromPreferences(String pConfigurationTopic) {
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
	    return preferences.getBoolean(
	    		pConfigurationTopic, false);
	}

    private void saveBooleanToPreferences(String pPreferenceTopic, boolean pValue) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(pPreferenceTopic, pValue);
        editor.apply();
    }

	private void sendLocationToCallingNumber(
			boolean pUseSMS, 
			boolean pUseInternet,
			String pCallerNumber) {

		// check if number is in current observing circle
		Log.d(getClass().toString(), "Sending location to calling number: " + pCallerNumber);
		
		// get the watching circles
		ArrayList<CircleItem> circles = loadCircleList();
		int observingCirclePosition = loadWatchingCirclePosition();
		
		// send the location if the caller is watching
		if (circles.size() > 0){
			CircleItem observingCircle = circles.get(observingCirclePosition);
			for (ContactInformation contact : observingCircle.getContacts()){
				// format the caller number
				String formatedNumber = contact.getPhoneNumber().replace(" ", "");
				
				if (formatedNumber.equals(pCallerNumber)){
					sendPositionToNumber(pUseSMS, pUseInternet, pCallerNumber);
				}
			}
		}
		
	}

	private void sendPositionToNumber(
			boolean pUseSMS, 
			boolean pUseInternet,
			String pCallerNumber) {
		
		if (pUseSMS){
			sendLocationSMS(pCallerNumber);
		}
		
		if (pUseInternet){
			sendLocationInternet(pCallerNumber);
		}
		
	}

    private void sendLocationToWatchingCircles(boolean pUseSMS, boolean pUseInternet) {
        sendLocationToWatchingCircles(pUseSMS, pUseInternet, false);
    }

	private void sendLocationToWatchingCircles(boolean pUseSMS, boolean pUseInternet, boolean pIsSOS) {

        if (pIsSOS){
            Log.d(getClass().toString(), "Sending SOS to watching circles");
        } else {
            Log.d(getClass().toString(), "Sending location to watching circles");
        }
		
		// get the watching circles
		ArrayList<CircleItem> circles = loadCircleList();
		int observingCirclePosition = loadWatchingCirclePosition();
		
		// send the location
		if (circles.size() > 0){
			CircleItem observingCircle = circles.get(observingCirclePosition);
			sendPositionToCircle(observingCircle, pUseSMS, pUseInternet, pIsSOS);
		}
		
	}

	private void sendPositionToCircle(
			CircleItem pObservingCircle,
			boolean pUseSMS, 
			boolean pUseInternet,
            boolean pIsSOS) {
		
		if (pObservingCircle.getContacts().isEmpty()){
			return;
		}
		
		if (pUseSMS || pIsSOS){
			sendLocationSMS(pObservingCircle, pIsSOS);
		} else {
			sendLocationInternet(pObservingCircle);
		}
	}

	/**
	 * Method send location to cloud
	 * @param pObservingCircle the observing circle to send the location to
	 */
	private void sendLocationInternet(CircleItem pObservingCircle) {
		
		String phoneNumber = getHostPhoneNumber();

		// check if object already exists
		ParseObject locationObject = getLocationObject(phoneNumber);
		if (locationObject == null){

			// create
			locationObject = new ParseObject("LocationObject");
		}
		
		locationObject.put(WebAPIConstants.LATITUDE_KEY, currentLatitude);
		locationObject.put(WebAPIConstants.LONGITUDE_KEY, currentLongitude);
		
		LinkedList<String> receivingNumbers = new LinkedList<String>();
		
		for (ContactInformation contact : pObservingCircle.getContacts()){
			receivingNumbers.add(contact.getPhoneNumber());
		}
		locationObject.put(WebAPIConstants.RECEIVING_PHONE_NUMBER_ARRAY, receivingNumbers);			
		locationObject.put(WebAPIConstants.SENDING_PHONE_NUMBER, phoneNumber);
		
		locationObject.saveInBackground();	
	}
	
	private ParseObject getLocationObject(String phoneNumber) {
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("LocationObject");
		query.whereEqualTo(WebAPIConstants.SENDING_PHONE_NUMBER, phoneNumber);
		try {
			List<ParseObject> result = query.find();
			if (result.size() == 0){
				return null;
			} else {
				return result.get(0);
			}
		} catch (ParseException e) {
			return null;
		}
	}

	private String getHostPhoneNumber() {
		
		SharedPreferences prefs = getSharedPreferences(
				ConfigurationTopics.USER_PHONE_CONFIG, 0);
		
		String number = prefs.getString(ConfigurationTopics.USER_PHONE_NUMBER, "");
		
		return number;
	}

	/**
	 * Method send location to cloud - just for a particular number
	 * @param pPhoneNumber the number to have access to the location
	 */
	private void sendLocationInternet(String pPhoneNumber) {
		
		ParseObject positionNotificationObject = new ParseObject("PositionNotificationObject");
		positionNotificationObject.put(WebAPIConstants.RECEIVING_PHONE_NUMBER, pPhoneNumber);
		positionNotificationObject.put(WebAPIConstants.LATITUDE_KEY, currentLatitude);
		positionNotificationObject.put(WebAPIConstants.LONGITUDE_KEY, currentLongitude);
		positionNotificationObject.saveInBackground();
		
	}

	private void sendLocationSMS(CircleItem pObservingCircle, boolean pIsSOS) {
		
		// send location to each contact in circle
		for( ContactInformation contact : pObservingCircle.getContacts()){

            // create the google maps link
            String link = createGoogleMapLink (currentLongitude, currentLatitude);

            if (pIsSOS){

                Log.d(getClass().toString(), "SOS SMS sent to watching circles");

                SmsManager.getDefault().sendTextMessage(
                        contact.getPhoneNumber(),
                        null,
                        "Pomocy!! \nMoja pozycja:\n długość " +
                                this.currentLongitude +
                                "\n szerokość: " +
                                this.currentLatitude +
                                "\n"+link,
                        null,
                        null);
            } else {
                SmsManager.getDefault().sendTextMessage(
                        contact.getPhoneNumber(),
                        null,
                        "Moja pozycja:\n długość " +
                                this.currentLongitude +
                                "\n szerokość: " +
                                this.currentLatitude +
                                "\n"+link,
                        null,
                        null);
            }
		}	
	}
	
	private void sendLocationSMS(String pNumber) {
		try{
			if (pNumber != null && !pNumber.equals("")){
				
				// create the google maps link
				String link = createGoogleMapLink (currentLongitude, currentLatitude);
				
				SmsManager.getDefault().sendTextMessage(
						pNumber, 
						null, 
						"Moja pozycja:\n długość " + 
						this.currentLongitude + "\n szerokość: " + this.currentLatitude + 
						"\n"+link, 
						null, 
						null);			
			}
		} catch (IllegalArgumentException exc){
			
		}
	}

	private String createGoogleMapLink(
			double pCurrentLongitude,
			double pCurrentLatitude) {
		
		//double[] googleCoords = MercatorTransform.forward(pCurrentLatitude, pCurrentLongitude);
		StringBuilder builder = new StringBuilder();
		builder.append("Google map link:\n");
		builder.append("http://maps.google.com/maps?q=loc:");
		builder.append(pCurrentLatitude);
		builder.append(",");
		builder.append(pCurrentLongitude);
		//builder.append("&z=13");
		
		return builder.toString();
	}

	private int loadWatchingCirclePosition() {
		
		// load the selected observing circle position
		SharedPreferences prefs = getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE);
		
		return prefs.getInt(ConfigurationTopics.SELECTED_OBSERVING_CIRLCE, 0);
	}

	private void terminateTimers() {
		
		//terminate timers
		if (smsTimer != null){
			smsTimer.cancel();
		}
		
		if (internetMessageTimer != null){
			internetMessageTimer.cancel();
		}
	}

	/**
	 * Method creates the timer responsible for fetching the position
	 * and sending it to various channels (SMS, Internet)
	 */
	private void scheduleLocationNotifications() {
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
	    numberInternetMessagesPerHour = preferences.getInt(
	    		ConfigurationTopics.LOCATION_SEND_INTERNET_MESSAGE_INTERVAL_INTENT_EXTRA, 6);
		
	    numberSMSMessagesPerHour = preferences.getInt(
	    		ConfigurationTopics.LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA, 6);
	    
	    boolean sendUpdatesViaSMS = 
	    		getBooleanFromPreferences(ConfigurationTopics.SEND_LOCATION_UPDATES_VIA_SMS);
	
		// tracking interval in milliseconds
		startTrackingPosition((60/numberInternetMessagesPerHour)*60000, true);
		startTrackingPosition((60/numberSMSMessagesPerHour)*60000, false);
		
		if (sendUpdatesViaSMS){
			scheduleSMSDispatch(numberSMSMessagesPerHour);
		}
		
		scheduleInternetMessageDispatch(numberInternetMessagesPerHour);
	}

	/**
	 * method sends the location notification to the central server
	 * @param pNumberInternetMessagesPerHour number of Internet notifications per hour
	 */
	private void scheduleInternetMessageDispatch(
			int pNumberInternetMessagesPerHour) {
		
		if (pNumberInternetMessagesPerHour != 0){
			internetMessageTimer = new Timer();
			
			internetMessageTimer.scheduleAtFixedRate(new TimerTask(){
				@Override
				public void run(){
					if (currentLatitude != 0 && currentLongitude != 0){
						Log.d(this.getClass().toString(), 
								"Internet message sent with location " + currentLongitude + 
								"; " + currentLatitude);
						
						sendLocationToWatchingCircles(false, true);
					}
				}
			}, 0, 3600000/pNumberInternetMessagesPerHour);
		}
		
	}

	/**
	 * method sends the SMS notification to current subscribers
	 * @param pNumberSMSMessagesPerHour number of SMS notifications per hour
	 */
	private void scheduleSMSDispatch(int pNumberSMSMessagesPerHour) {
		if (pNumberSMSMessagesPerHour != 0){
			smsTimer = new Timer();
			
			smsTimer.scheduleAtFixedRate(new TimerTask(){
				@Override
				public void run(){
					if (currentLatitude != 0 && currentLongitude != 0){
						Log.d(this.getClass().toString(), 
								"SMS sent with location " + currentLongitude + 
								"; " + currentLatitude);
						
						sendLocationToWatchingCircles(true, false);
					}
				}
			}, 0, 3600000/pNumberSMSMessagesPerHour);
		}
	}

	@Override
	public void onCreate(){
		
		// inicjalizacja menadżera lokalizacji
		initializeLocationManager();
		
		// setup Parse
        Log.d(this.getClass().toString(), "setting up Parse");

        Parse.initialize(this, "sOsBqqe5p4RUivGYhFH5x6WOtFCHawnYHhbNdtZa",
        "KCsoM8ABjcUi8SbJTEa03vURaSjIqEJe0jJN4T8R");
		
		//create hook for shared preferences changes
		PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        Log.d(this.getClass().toString(), "setting up notifications");

        setupNotifications();
	}

	private void setupNotifications() {
		
		terminateTimers();
		scheduleLocationNotifications();	
	}

	@Override
	public IBinder onBind(Intent intent) {
		serviceBound = true;
		return mBinder;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		serviceBound = false;
		return false;
	};
	
	@Override
	public void onRebind(Intent intent) {
		serviceBound = true;
	};
	
	@Override
	public void onLocationChanged(Location location) {
		handleLocationUpdate(location);
	}

	private void handleLocationUpdate(Location pLocation) {

		if (null != pLocation) {
			if (pLocation.getAccuracy() <= currentBestAccuracy) {
				Log.d(this.getClass().toString(), "Got location fix from "
						+ pLocation.getProvider() + " provider.");
				currentBestAccuracy = pLocation.getAccuracy();
				
				currentLatitude = pLocation.getLatitude();
				currentLongitude = pLocation.getLongitude();
				
				// should we broadcast the position to the app?
				if (serviceBound){
					
					Log.d(this.getClass().toString(),
							"Sending location notification to main app");
										
					Intent intent = new Intent(IntentMessages.LOCATION_UPDATE);
					intent.putExtra(IntentMessages.LOCATION_UPDATE_PARCEL, 
							pLocation);
					LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
				}
			}
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d(this.getClass().toString(), "Enabled new provider " + provider);;

	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d(this.getClass().toString(), "Disabled provider " + provider);;

	}
	
	private void initializeLocationManager() {

		Log.d(this.getClass().toString(), "Initializing Location Manager");
		
		// Get the location manager
		locationManager = (LocationManager) getSystemService(
				Context.LOCATION_SERVICE);
		
		// Define the criteria how to select the location provider -> use
		// default
		Criteria criteria = new Criteria();
		criteria.setSpeedAccuracy(Criteria.NO_REQUIREMENT);
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		locationProvider = locationManager.getBestProvider(criteria, false);

		Log.d(this.getClass().toString(), "Selected location provider: "
				+ locationProvider);
				
		// check if GPS enabled
		performGPSCheck(locationManager, locationProvider);
	}
	
	// method checks if GPS is enabled on device
	// if not prompts the user to activate the GPS and sends him to the settings
	// window
	private void performGPSCheck(LocationManager pLocationManager,
			String pLocationProvider) {

		boolean enabled = pLocationManager.isProviderEnabled(pLocationProvider);

		if (!enabled) {

			Log.d(this.getClass().toString(),
					"Location services not enabled asking user for action...");
			Intent intent = new Intent(this, ServiceBroadcastReceiver.class);
			intent.setAction(IntentMessages.GPS_ACTIVATION_REQUEST);
			LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
			
		}
	}
	
	@Override
	public void onDestroy(){
		
		//unregister hook for shared preferences changes
		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
		
		// terminate location updates
		locationManager.removeUpdates(this);
		
		//terminate timers
		terminateTimers();
	}
	
	/**
	 * start tracking
	 * @param pLocationUpdateTime update interval in milliseconds
	 */
	private void startTrackingPosition(long pLocationUpdateTime, boolean pRemovePreviousUpdates) {
		
		if (pRemovePreviousUpdates){
			// terminate previous location updates
			locationManager.removeUpdates(this);
		}
		
		// if tracking enabled launch the location requests
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, pLocationUpdateTime, 1,
				this);
		locationManager.requestLocationUpdates(locationProvider,
				pLocationUpdateTime, 1, this);

		currentBestAccuracy = Float.MAX_VALUE;
	}
	
	private ArrayList<CircleItem> loadCircleList() {
		
		Gson gson = new Gson();
		String circleListGson = getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE).getString(ConfigurationTopics.CIRCLE_LIST_PREFERENCE_KEY, "");
		
		ArrayList<CircleItem> deserializedCircleList = new ArrayList<CircleItem>();
		
		if (!circleListGson.equals("")){
			Type listOfTestObject = new TypeToken<ArrayList<CircleItem>>(){}.getType();
			deserializedCircleList = gson.fromJson(circleListGson, listOfTestObject);
		}
		
		return deserializedCircleList;
	}
	
	public class PeopleFinderServiceBinder extends Binder {
		
		public PeopleFinderService getService(){
			return PeopleFinderService.this;
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		
		if (key.equals(ConfigurationTopics.LOCATION_SEND_INTERNET_MESSAGE_INTERVAL_INTENT_EXTRA) ||
				key.equals(ConfigurationTopics.LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA) ||
				key.equals(ConfigurationTopics.SEND_LOCATION_UPDATES_VIA_SMS)){
			
			// reset the notification logic - parameters changed
			setupNotifications();
		}	
	}
}
