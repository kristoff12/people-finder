package com.insomnia.peoplelocator.tracking;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.insomnia.peoplelocator.config.IntentMessages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Krzychu on 30.03.2015.
 */
public class FetchAddressIntentService extends IntentService{

    private Geocoder geocoder;
    private final String TAG = this.getClass().getName();
    protected ResultReceiver mReceiver;


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public FetchAddressIntentService(String name) {
        super(name);
    }

    public FetchAddressIntentService() {
        super("");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        geocoder = new Geocoder(this, Locale.getDefault());
        String errorMessage = "";

        // get the receiver
        mReceiver = intent.getParcelableExtra(IntentMessages.RECEIVER);

        // Get the location passed to this service through an extra.
        double[] location = intent.getDoubleArrayExtra(
                IntentMessages.LOCATION_DATA_EXTRA);

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location[0],
                    location[1],
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = "network not enabled";
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = "invalid coordinates";
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location[0] +
                    ", Longitude = " +
                    location[1], illegalArgumentException);
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = "no address found";
                Log.e(TAG, errorMessage);
            }
            deliverResultToReceiver(IntentMessages.FAILURE_RESULT, errorMessage);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            Log.d(TAG, "address decoded");
            deliverResultToReceiver(IntentMessages.SUCCESS_RESULT,
                    TextUtils.join(System.getProperty("line.separator"),
                            addressFragments));
        }
    }

    private void deliverResultToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString(IntentMessages.RESULT_DATA_KEY, message);
        mReceiver.send(resultCode, bundle);
    }
}
