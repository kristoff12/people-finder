package com.insomnia.peoplelocator.tracking;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

public class PeopleFinderServiceConnection implements ServiceConnection{
	
	private PeopleFinderService mPeopleFinderService;
	
	public void onServiceConnected(ComponentName className, IBinder binder) {

		PeopleFinderService.PeopleFinderServiceBinder serviceBinder = (PeopleFinderService.PeopleFinderServiceBinder) binder;

		mPeopleFinderService = serviceBinder.getService();
		Log.d(this.getClass().toString(),"Connected to tracking service");
	}

	public void onServiceDisconnected(ComponentName className) {

		mPeopleFinderService = null;
	}
	
	public PeopleFinderService getService(){
		return mPeopleFinderService;
	}

}
