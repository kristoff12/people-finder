package com.insomnia.peoplelocator.tracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;

import com.insomnia.peoplelocator.TrackingStatusFragment;
import com.insomnia.peoplelocator.config.IntentMessages;

public class ServiceBroadcastReceiver extends BroadcastReceiver{

	TrackingStatusFragment mTrackingStatusFragment;

    public ServiceBroadcastReceiver(){}

	public ServiceBroadcastReceiver(TrackingStatusFragment pTrackingStatusFragmentInstance){
		mTrackingStatusFragment = pTrackingStatusFragmentInstance;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (null != intent && mTrackingStatusFragment != null){
			
			if (intent.getAction().equals(IntentMessages.GPS_ACTIVATION_REQUEST)){
				mTrackingStatusFragment.promptUserToActivateGPS();
			} else if (intent.getAction().equals(IntentMessages.LOCATION_UPDATE)){
				Location location = (Location)intent.getParcelableExtra(IntentMessages.LOCATION_UPDATE_PARCEL);
				mTrackingStatusFragment.handleLocationUpdate(location);
				
				// forward to maps
				intent.setAction(IntentMessages.HostLocation);
				LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
				
			} else if (intent.getAction().equals(IntentMessages.USER_REFUSED_ACTIVATING_LOCATION_SERVICES)){
				mTrackingStatusFragment.handleUserRefusedToActivateGPS();
			}
		}
	}
		
}
