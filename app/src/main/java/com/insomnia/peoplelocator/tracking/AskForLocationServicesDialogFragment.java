package com.insomnia.peoplelocator.tracking;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;

import com.insomnia.peoplelocator.R;
import com.insomnia.peoplelocator.config.IntentMessages;

public class AskForLocationServicesDialogFragment extends DialogFragment{
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.ask_user_location_services)
               .setPositiveButton(R.string.ask_user_location_services_yes, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   
     				  Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    				  startActivity(intent);
                   }
               })
               .setNegativeButton(R.string.ask_user_location_services_no, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // User cancelled the dialog
                	   //broadcast an appropriate message
                	   Intent intent = new Intent(getActivity(), ServiceBroadcastReceiver.class);
                	   intent.setAction(IntentMessages.USER_REFUSED_ACTIVATING_LOCATION_SERVICES);
                	   LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                   }
               }).setTitle(R.string.ask_user_location_services_title);
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
