package com.insomnia.peoplelocator.map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.insomnia.peoplelocator.LocationMapFragment;
import com.insomnia.peoplelocator.config.IntentMessages;

public class MapBroadcastReceiver extends BroadcastReceiver{

	LocationMapFragment mLocationMapFragment;

    public MapBroadcastReceiver(){}

	public MapBroadcastReceiver(LocationMapFragment pLocationMapFragment){
		mLocationMapFragment = pLocationMapFragment;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (null != intent && mLocationMapFragment != null){
			
			if (intent.getAction().equals(IntentMessages.HostLocation)){
				Location location = (Location)intent.getParcelableExtra(IntentMessages.LOCATION_UPDATE_PARCEL);
				mLocationMapFragment.updateHostLocation(location);
			}
		}
	}
}
