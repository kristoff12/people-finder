package com.insomnia.peoplelocator.config;

public class IntentMessages {

	public static final String LOCATION_UPDATE = "LOCATION_UPDATE";
	public static final String GPS_ACTIVATION_REQUEST = "GPS_ACTIVATION_REQUEST";
	public static final String LOCATION_UPDATE_PARCEL = "LOCATION_UPDATE_PARCEL";
	public static final String USER_REFUSED_ACTIVATING_LOCATION_SERVICES = "USER_REFUSED_ACTIVATING_LOCATION_SERVICES";
	public static final String CircleItem = "CIRCLE_ITEM";
	public static final String PERSON_LOCATION_PARCEL = "PERSON_LOCATION_PARCEL";
	public static final String HostLocation = "HOST_LOCATION";
	public static final String TrackedPersonLocation = "TRACKED_PERSON_LOCATION";


    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME =
            "com.insomnia.location";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";
}
