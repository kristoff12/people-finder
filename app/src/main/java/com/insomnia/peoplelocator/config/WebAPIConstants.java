package com.insomnia.peoplelocator.config;

public class WebAPIConstants {

	public static final String LATITUDE_KEY = "LATITUDE_KEY";
	public static final String LONGITUDE_KEY = "LONGITUDE_KEY";
	public static final String RECEIVING_PHONE_NUMBER = "RECEIVING_PHONE_NUMBER";
	public static final String RECEIVING_PHONE_NUMBER_ARRAY = "RECEIVING_PHONE_NUMBER_ARRAY";
	public static final String SENDING_PHONE_NUMBER = "SENDING_PHONE_NUMBER";
    public static final String SOS_SIGNAL_FLAG = "SOS_SIGNAL_FLAG";
}
