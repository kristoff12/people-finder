package com.insomnia.peoplelocator.config;


public class ConfigurationTopics {

	public static final String STORED_LONGITUDE = "STORED_LONGITUDE";
	public static final String TRACKING_ENABLED = "TRACKING_ENABLED";
	public static final String STORED_LATITUDE = "STORED_LATITUDE";
	public static final String LOCATION_UPDATE_INTERVAL = "LOCATION_UPDATE_INTERVAL";
	public static final String LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA = "LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA";
	public static final String LOCATION_SEND_INTERNET_MESSAGE_INTERVAL_INTENT_EXTRA = "LOCATION_SEND_INTERNET_MESSAGE_INTERVAL_INTENT_EXTRA";
	public static final String LOCATION_SEND_SMS_AFTER_RING_NUMBER = "LOCATION_SEND_SMS_AFTER_RING_NUMBER";
	public static final String SEND_SMS_ON_CALL_NO_ANSWER = "SEND_SMS_ON_CALL_NO_ANSWER";
	public static final String SEND_LOCATION_UPDATES_VIA_SMS = "SEND_LOCATION_UPDATES_VIA_SMS";
	
	// intent extra config
	public static final String SEND_LOCATION_TO_CIRCLE_AFTER_CALL = "SEND_LOCATION_TO_CIRCLE_AFTER_CALL";
	/**
	 * The prefered method to send location updates:
	 * <b>0</b> - internet
	 * <b>1</b> - sms
	 */
	public static final String PREFERED_LOCATION_SEND_METHOD_TYPE = "PREFERED_LOCATION_SEND_METHOD_TYPE";
	
	// shared preferences - storing circle items
	public static final String CIRCLE_ITEM_PREFERENCES_KEY = "CIRCLE_ITEM";
	public static final String CIRCLE_LIST_PREFERENCE_KEY = "CIRCLE_LIST";
	public static final String CIRCLE_SHARED_PREFERENCES = "CIRCLE_SHARED_PREFERENCES";
	public static final String WATCHING_CIRCLE_LIST_PREFERENCE_KEY = "WATCHING_CIRCLE_LIST_PREFERENCE_KEY";
	public static final String SELECTED_OBSERVING_CIRLCE = "SELECTED_OBSERVING_CIRLCE";
	public static final String CALLER_NUMBER = "CALLER_NUMBER";
	
	// shared preferences names
	public static final String USER_PHONE_CONFIG = "USER_PHONE_CONFIG";
	public static final String USER_PHONE_NUMBER = "USER_PHONE_NUMBER";
    public static final String SEND_SOS_MESSAGE = "SEND_SOS_MESSAGE";
}
