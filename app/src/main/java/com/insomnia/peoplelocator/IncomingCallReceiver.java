package com.insomnia.peoplelocator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.tracking.PeopleFinderService;

import java.util.Date;

public class IncomingCallReceiver extends BroadcastReceiver{

	private static long callTime = 0;
	private long answeredTime = 0;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
		
		if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
		    //Phone ringing
		    callTime = new Date().getTime();

		}else if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
			
		}else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)){
			if (callTime > 0){
				
			    //Call answered
			    answeredTime = new Date().getTime();
			    long timeTaken = answeredTime - callTime;
			    
			    Log.d(this.getClass().toString(), "call answered after " + timeTaken/ 1000 + " seconds");
			    
			    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			    int sendMessageThreshold = preferences.getInt(
			    		ConfigurationTopics.LOCATION_SEND_SMS_AFTER_RING_NUMBER, 0);
			    
			    if (sendMessageThreshold != 0 &&
			    		timeTaken/1000 >= sendMessageThreshold){
			    	// send the location to observing circle
			    	Intent serviceIntent = new Intent(context, PeopleFinderService.class);
			    	serviceIntent.putExtra(ConfigurationTopics.SEND_LOCATION_TO_CIRCLE_AFTER_CALL, true);
			    	
			    	Bundle bundle = intent.getExtras();
			        String phoneNr= bundle.getString("incoming_number");
			        
			    	serviceIntent.putExtra(ConfigurationTopics.CALLER_NUMBER, phoneNr);
			    	context.startService(serviceIntent);
			    }
			}

		}
		
	}

}
