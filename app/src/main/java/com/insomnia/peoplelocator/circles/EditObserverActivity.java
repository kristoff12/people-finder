package com.insomnia.peoplelocator.circles;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.insomnia.peoplelocator.CircleItem;
import com.insomnia.peoplelocator.ContactInformation;
import com.insomnia.peoplelocator.R;
import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.config.IntentMessages;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class EditObserverActivity extends Activity{

	private final int contactActivityRequestCode = 1;
	private CircleItem circleItem;
	
	// circle list adapter
	private ContactInformationListAdapter circleItemListAdapter;
	
	private ListView circleItemListView;

    private TextView hintTextView;

    private LinearLayout viewLayout;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_observer_layout);
		
		// get the CircleItem we are editing
		 circleItem = (CircleItem)getIntent().getExtras().get(IntentMessages.CircleItem);

        viewLayout = (LinearLayout)findViewById(R.id.edit_observers_layout);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		circleItemListView = (ListView)findViewById(R.id.observersListView);
		
		circleItemListAdapter = new ContactInformationListAdapter(this, circleItem.getContacts());
		circleItemListView.setAdapter(circleItemListAdapter);
		
		// contextual menu
		circleItemListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		circleItemListView.setMultiChoiceModeListener(new CircleItemMultiChoiceListener());
		
		TextView textView = (TextView)findViewById(R.id.edit_observer_header);
		textView.setText(circleItem.getCircleName());

        // no contacts present
        if (circleItem.getContacts().size() == 0){

            // show hint
            showHintTextView();
        }
	}
	
	@Override
	protected void onPause() {
	
		super.onPause();
		
		// read the circle items stored
		ArrayList<CircleItem> circleItems = loadCircleList();
		
		// find the circle item edited
		for (CircleItem c: circleItems){
			if (c.getId().equals(this.circleItem.getId())){
				c.setContacts(this.circleItem.getContacts());
			}
		}
				
		//save the whole structure
		Gson gson = new Gson();
		String circleListGson = gson.toJson(circleItems);
		
		Editor editor = getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE).edit();
		
		editor.putString(ConfigurationTopics.CIRCLE_LIST_PREFERENCE_KEY, circleListGson);
		editor.commit();
		
	}
	
	public void addObserverButtonClicked (View view){
		// construct intent for the address book activity
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, contactActivityRequestCode);
	}

    private void showHintTextView() {

        // hide the list view
        circleItemListView.setVisibility(View.INVISIBLE);

        hintTextView = new TextView(this);
        hintTextView.setText(R.string.addContactToCircleHintText);
        hintTextView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        hintTextView.setGravity(Gravity.CENTER);
        hintTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        viewLayout.addView(hintTextView);

    }
	
	@Override 
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);
		switch (reqCode){
		case contactActivityRequestCode:
			
			if (resultCode == Activity.RESULT_OK){
				Uri contactData = data.getData();
				Cursor c = getContentResolver().query(contactData, null, null, null, null);
				
				if (c.moveToFirst()){
					
					String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
					String phoneNumber = null;
						
					String contactId= c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
					
					Cursor phones = getContentResolver().query(Phone.CONTENT_URI, null,
					        Phone.CONTACT_ID + " = " + contactId, null, null);
					
					boolean isFound = false;
					
				    while (!isFound && phones.moveToNext()) {
				        String number = phones.getString(phones.getColumnIndex(Phone.NUMBER));
				        int type = phones.getInt(phones.getColumnIndex(Phone.TYPE));
				        switch (type) {
				            case Phone.TYPE_HOME:
				            	phoneNumber = number;
				            	isFound = true;
				            	break;
				            case Phone.TYPE_MOBILE:
				            	phoneNumber = number;
				            	isFound = true;
				            	break;
				            case Phone.TYPE_WORK:
				            	phoneNumber = number;
				            	isFound = true;
				            	break;
				        }
				    }
				    phones.close();

					Log.d(getClass().toString(), "Loaded contact: " + name);
					ContactInformation contactInfo = new ContactInformation(name);
					if (phoneNumber != null){
						contactInfo.setPhoneNumber(phoneNumber);
					}
					contactInfo.setContactID(Long.parseLong(contactId));
					circleItem.addContact(contactInfo);

                    // hide the hint text view and show the list view
                    hintTextView.setVisibility(View.INVISIBLE);
                    circleItemListView.setVisibility(View.VISIBLE);

					circleItemListAdapter.notifyDataSetChanged();
				}
			}
			break;
		}
	}
	
	 public Bitmap loadContactImage(long contactId) {
	     Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
	     Uri photoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.CONTENT_DIRECTORY);
	     Cursor cursor = getContentResolver().query(photoUri,
	          new String[] {Contacts.Photo.PHOTO}, null, null, null);
	     if (cursor == null) {
	         return null;
	     }
	     try {
	         if (cursor.moveToFirst()) {
	             byte[] data = cursor.getBlob(0);
	             if (data != null) {
	                 return BitmapFactory.decodeByteArray(data, 0, data.length);
	             }
	         }
	     } finally {
	         cursor.close();
	     }
	     return null;
	 }
	
	public void deleteSelectedItems(){
		
		SparseBooleanArray array = circleItemListAdapter.checkedItemsArray.clone();
		ArrayList<ContactInformation> contactInformationList = new ArrayList<ContactInformation>(); 
		
		for( int i=0; i< array.size(); ++i){
			int position = circleItemListAdapter.checkedItemsArray.keyAt(i);
			contactInformationList.add(circleItem.getContact(position));
			circleItemListAdapter.checkedItemsArray.delete(position);
		}
		circleItem.removeContacts(contactInformationList);
		circleItemListAdapter.notifyDataSetChanged();
	}
	
	public void itemChecked( View view, int position) {
		
		for( int i=0; i< circleItemListAdapter.checkedItemsArray.size(); ++i){
			if (circleItemListAdapter.checkedItemsArray.get((Integer)view.getTag()) == true){
				circleItemListView.setItemChecked(position, true);
			}
		}
	}
	
	private ArrayList<CircleItem> loadCircleList() {
		
		Gson gson = new Gson();
		String circleListGson = getSharedPreferences(
				ConfigurationTopics.CIRCLE_SHARED_PREFERENCES, 
				Context.MODE_PRIVATE).getString(ConfigurationTopics.CIRCLE_LIST_PREFERENCE_KEY, "");
		
		ArrayList<CircleItem> deserializedCircleList = new ArrayList<CircleItem>();
		
		if (!circleListGson.equals("")){
			Type listOfTestObject = new TypeToken<ArrayList<CircleItem>>(){}.getType();
			deserializedCircleList = gson.fromJson(circleListGson, listOfTestObject);
		}
		
		return deserializedCircleList;
	}
	
	// adapter class
	public class ContactInformationListAdapter extends ArrayAdapter <ContactInformation> implements OnCheckedChangeListener{

		private Context context;
		private ArrayList<ContactInformation> contactInfoList;
		public SparseBooleanArray checkedItemsArray;

		public ContactInformationListAdapter (Context context, ArrayList<ContactInformation> contactInfoList){
			super(context, R.layout.circle_item_layout, contactInfoList);
			this.context = context;
			this.contactInfoList = contactInfoList;	
			checkedItemsArray = new SparseBooleanArray(contactInfoList.size());
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent){
			
			View rowView;
			if (convertView != null){
				rowView = convertView;
			} else {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.observer_item_layout, parent, false);
			}
			
			TextView firstLineTextView = (TextView)rowView.findViewById(R.id.firstLine);
			TextView secondLineTextView = (TextView)rowView.findViewById(R.id.secondLine);
			CheckBox checkbox = (CheckBox)rowView.findViewById(R.id.checkBox1);
			checkbox.setOnCheckedChangeListener(this);
			
			firstLineTextView.setText(contactInfoList.get(position).getContactName());
			secondLineTextView.setText(contactInfoList.get(position).getPhoneNumber());
			
			ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
			
			Bitmap bitmap = loadContactImage(contactInfoList.get(position).getContactID());
			imageView.setImageBitmap(bitmap);
			
			rowView.setTag((Integer)position);
			
			return rowView;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			Object tag = ((View)buttonView.getParent()).getTag();
			
			checkedItemsArray.put((Integer)tag, isChecked);
			((EditObserverActivity)context).itemChecked(
					((View)buttonView.getParent()), 
					(Integer)tag);
			
		}
		
	}
	
	public class CircleItemMultiChoiceListener implements MultiChoiceModeListener{
		
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
	        // Inflate the menu for the CAB
	        MenuInflater inflater = mode.getMenuInflater();
	        inflater.inflate(R.menu.context_menu, menu);
	        
	        return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
	        // Respond to clicks on the actions in the CAB
	        switch (item.getItemId()) {
	            case R.id.context_menu_contactinfo_remove:
	                deleteSelectedItems();
	                mode.finish(); // Action picked, so close the CAB
	                return true;
	            default:
	                return false;
	        }
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
		}

		@Override
		public void onItemCheckedStateChanged(ActionMode mode, int position,
				long id, boolean checked) {
			
		}
	}
}
