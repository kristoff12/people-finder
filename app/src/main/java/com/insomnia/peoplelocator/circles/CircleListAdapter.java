package com.insomnia.peoplelocator.circles;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.insomnia.peoplelocator.CircleItem;
import com.insomnia.peoplelocator.ObserversConfigurationActivity;
import com.insomnia.peoplelocator.R;

import java.util.ArrayList;

public class CircleListAdapter extends ArrayAdapter <CircleItem> implements OnClickListener{

	private Context context;
	private ArrayList<CircleItem> circleList;
	private int selectedPostion = 0;

	public CircleListAdapter (Context context, ArrayList<CircleItem> circleList, int pSelectedPosition){
		super(context, R.layout.circle_item_layout, circleList);
		this.context = context;
		this.circleList = circleList;	
		this.selectedPostion = pSelectedPosition;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View rowView = convertView;
		
		if (rowView == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.circle_item_layout, parent, false);
		}
		
		TextView firstLineTextView = (TextView)rowView.findViewById(R.id.firstLine);
		TextView secondLineTextView = (TextView)rowView.findViewById(R.id.secondLine);
		
		firstLineTextView.setText(circleList.get(position).getCircleName());
		secondLineTextView.setText(String.valueOf(circleList.get(position).getNumberOfPeopleInCircle()));
		
		ToggleButton toggleButton = (ToggleButton)rowView.findViewById(R.id.checkBox1);
		toggleButton.setChecked(position == selectedPostion);
		toggleButton.setOnClickListener(this);
		
		toggleButton.setTag((Integer)position);
		
		ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
		imageView.setImageResource (R.drawable.group_icon);
		
		return rowView;
	}
	
	@Override
	public void onClick(View v) {
		
		selectedPostion = (Integer)v.getTag();
		
		((ObserversConfigurationActivity)context).itemChecked(
				v, 
				selectedPostion,
				true);	
		
		notifyDataSetInvalidated();
		
	}
	
}
