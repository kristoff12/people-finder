package com.insomnia.peoplelocator.splashscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.insomnia.peoplelocator.R;
import com.insomnia.peoplelocator.TabbedActivity;

/**
 * Created by Krzychu on 29.03.2015.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.splashscreen_layout);

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // start main activity
        Intent intent = new Intent(this, TabbedActivity.class);
        startActivity(intent);

        // close the login activity
        this.finish();
    }
}
