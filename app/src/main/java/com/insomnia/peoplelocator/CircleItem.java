package com.insomnia.peoplelocator;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Class represents the circle item
 * @author Krzysiek
 *
 */
public class CircleItem implements Parcelable{
	String mCircleName;
	ArrayList<ContactInformation> mContactList;
	String mId;
	
	public CircleItem (String pName){
		mCircleName = pName;
		mContactList = new ArrayList<ContactInformation>();
		mId = UUID.randomUUID().toString();
	}
	
	public CircleItem(Parcel source) {
		
		mCircleName = source.readString();
		mId = source.readString();
		mContactList = new ArrayList<ContactInformation>();
		source.readList(mContactList, ContactInformation.class.getClassLoader());
	}

	public String getCircleName(){
		return mCircleName;
	}
	
	public void addContact(ContactInformation pContact){
		mContactList.add(pContact);
	}
	
	public int getNumberOfPeopleInCircle(){
		return mContactList.size();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mCircleName);
		dest.writeString(mId);
		dest.writeList(mContactList);	
	}
	
	public static final Parcelable.Creator<CircleItem> CREATOR = new Parcelable.Creator<CircleItem>() {

		@Override
		public CircleItem createFromParcel(Parcel source) {
			return new CircleItem(source);
		}

		@Override
		public CircleItem[] newArray(int size) {
			return new CircleItem[size];
		}
	};

	public ArrayList<ContactInformation> getContacts() {

		return mContactList;
	}

	public ContactInformation getContact(int position) {
		if (position < mContactList.size()){
			return mContactList.get(position);
		} else {
			return null;
		}
	}

	public void removeContacts(
			ArrayList<ContactInformation> contactInformationList) {
		
		mContactList.removeAll(contactInformationList);
		
	}
	
	public String getId(){
		return mId;
	}

	public void setContacts(ArrayList<ContactInformation> contacts) {
		this.mContactList = contacts;
		
	}
}
