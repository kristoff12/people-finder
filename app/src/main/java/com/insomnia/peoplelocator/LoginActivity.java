package com.insomnia.peoplelocator;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.insomnia.peoplelocator.common.PhoneNumberConverter;
import com.insomnia.peoplelocator.config.ConfigurationTopics;
import com.insomnia.peoplelocator.splashscreen.SplashActivity;

public class LoginActivity extends Activity{
	
	private EditText userPhoneNumberEditText;
	private EditText userPhoneNumberDirectionEditText;
	private TextView errorLoginInfoText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		setContentView(R.layout.welcome_layout);
		
		userPhoneNumberEditText = (EditText)findViewById(R.id.userPhoneNumber);
		errorLoginInfoText = (TextView)findViewById(R.id.errorLoginInfoText);
		userPhoneNumberDirectionEditText = (EditText)findViewById(R.id.UserPhoneNumberDirection);
		
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		// check if configuration already done
		SharedPreferences prefs = getSharedPreferences(ConfigurationTopics.USER_PHONE_CONFIG, 0);
		if (prefs.contains(ConfigurationTopics.USER_PHONE_NUMBER)){
			
			// already configured - skip
			
			// start main activity
			Intent intent = new Intent(this, SplashActivity.class);
			startActivity(intent);
			
			// close the login activity
			this.finish();
		}
	}
	
	public void performLogin (View view){
		
		String userNumber = userPhoneNumberEditText.getText().toString();
		String userNumberDirection = userPhoneNumberDirectionEditText.getText().toString();
		SharedPreferences prefs = getSharedPreferences(ConfigurationTopics.USER_PHONE_CONFIG, 0);
		Editor prefEditor = prefs.edit();
		
		// perform filtering - no spaces & no +xx only last 9 numbers
		try{
			userNumber = PhoneNumberConverter.convertPhoneNumber(userNumberDirection, userNumber);
			
			prefEditor.putString(ConfigurationTopics.USER_PHONE_NUMBER, userNumber);	
			prefEditor.commit();
			
			// start main activity
			Intent intent = new Intent(this, TabbedActivity.class);
			startActivity(intent);
			
			// close the login activity
			this.finish();
		
		} catch (Exception e){
			// something wrong with the input
			// notify the user
			errorLoginInfoText.setText("Hum.. Could you try again?");
		}
	}

}
