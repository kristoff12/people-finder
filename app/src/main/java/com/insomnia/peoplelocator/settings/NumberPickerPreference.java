package com.insomnia.peoplelocator.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.NumberPicker;

/**
 * A {@link android.preference.Preference} that displays a number picker as a dialog.
 */
public class NumberPickerPreference extends DialogPreference {

    public static final int MAX_VALUE = 100;
    public static final int MIN_VALUE = 0;

    private NumberPicker picker;
    private int value;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NumberPickerPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    
    @Override
    protected View onCreateView(ViewGroup parent) {
    	String key = getKey();
    	String summary= "";
    	
    	if (key.equals("LOCATION_UPDATE_INTERVAL")){
    		summary = "Update location every " + this.getValue() + " seconds";
    	} else if (key.equals("LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA")){
    		summary = "Send SMS message with location to selected circle every " + this.getValue() + " seconds";
    	} else if (key.equals("LOCATION_SEND_INTERNET_MESSAGE_INTERVAL_INTENT_EXTRA")){
    		summary = "Send location via Internet to selected circle every " + this.getValue() + " seconds";
    	} else if (key.equals("LOCATION_SEND_SMS_AFTER_RING_NUMBER")){
    		summary = "Send location to selected circle after " + this.getValue() + " rings";
    	}
    	this.setSummary(summary);
    	return super.onCreateView(parent);
    }

    @Override
    protected View onCreateDialogView() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;

        picker = new NumberPicker(getContext());
        picker.setLayoutParams(layoutParams);

        FrameLayout dialogView = new FrameLayout(getContext());
        dialogView.addView(picker);

        return dialogView;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        picker.setMinValue(MIN_VALUE);
        picker.setMaxValue(MAX_VALUE);
        picker.setValue(getValue());
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            setValue(picker.getValue());
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, MIN_VALUE);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        setValue(restorePersistedValue ? getPersistedInt(MIN_VALUE) : (Integer) defaultValue);
    }

    public void setValue(int value) {
        this.value = value;
        persistInt(this.value);
    }

    public int getValue() {
        return this.value;
    }
}
