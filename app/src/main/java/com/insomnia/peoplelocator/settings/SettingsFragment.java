package com.insomnia.peoplelocator.settings;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.insomnia.peoplelocator.R;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener{
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    	
    	// handle the send updates with sms
    	configureSendSMSUpdatesOptions();
    }
    
    private void configureSendSMSUpdatesOptions() {
    	CheckBoxPreference checkboxPreference = (CheckBoxPreference)findPreference("SEND_LOCATION_UPDATES_VIA_SMS");
    	Preference smsIntervalPreference = findPreference("LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA");
    	smsIntervalPreference.setEnabled(checkboxPreference.isChecked());
		
	}

	@Override
    public void onPause() {

    	super.onPause();
    	getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String pKey) {
		
		Preference preference = findPreference(pKey);
		if (preference instanceof NumberPickerPreference){
			NumberPickerPreference numberPickerPreference = (NumberPickerPreference) preference;
	    	String key = numberPickerPreference.getKey();
	    	String summary= "";
	    	
	    	if (key.equals("LOCATION_UPDATE_INTERVAL")){
	    		summary = "Update location every " + numberPickerPreference.getValue() + " minutes";
	    	} else if (key.equals("LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA")){
	    		summary = "Send SMS message with location to selected circle every " + numberPickerPreference.getValue() + " minutes";
	    	} else if (key.equals("LOCATION_SEND_INTERNET_MESSAGE_INTERVAL_INTENT_EXTRA")){
	    		summary = "Send location via Internet to selected circle every " + numberPickerPreference.getValue() + " minutes";
	    	} else if (key.equals("LOCATION_SEND_SMS_AFTER_RING_NUMBER")){
	    		summary = "Send location to selected circle after " + numberPickerPreference.getValue() + " rings";
	    	}
	    	numberPickerPreference.setSummary(summary);
		}
		
		// if send updates with SMS is disabled disable the sms update 
		// interval option for clarity
		if (pKey.equals("SEND_LOCATION_UPDATES_VIA_SMS")){
			CheckBoxPreference checkboxPreference = (CheckBoxPreference) preference;
			Preference smsSendingPreference = findPreference("LOCATION_SEND_SMS_INTERVAL_INTENT_EXTRA");
			if (!checkboxPreference.isChecked()){
				smsSendingPreference.setEnabled(false);
			} else {
				smsSendingPreference.setEnabled(true);
			}
		}
		
	}
}
